#ifndef TESTINGCLASS_H
#define TESTINGCLASS_H

#include <QObject>

class TestingClass : public QObject
{
    Q_OBJECT
public:
    explicit TestingClass(QObject *parent = nullptr);
    int GetMin(int first, int second);
    int GetMax(int first, int second);

private:


signals:

public slots:
};

#endif // TESTINGCLASS_H
