#include <QtTest>
#include <QCoreApplication>

// add necessary includes here

class NewMoreTesting : public QObject
{
    Q_OBJECT

public:
    NewMoreTesting();
    ~NewMoreTesting();

private slots:
    void initTestCase();
    void cleanupTestCase();
    void test_case1();

};

NewMoreTesting::NewMoreTesting()
{

}

NewMoreTesting::~NewMoreTesting()
{

}

void NewMoreTesting::initTestCase()
{

}

void NewMoreTesting::cleanupTestCase()
{

}

void NewMoreTesting::test_case1()
{

}

QTEST_MAIN(NewMoreTesting)

#include "tst_newmoretesting.moc"
