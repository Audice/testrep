#include <QtTest>
#include <QCoreApplication>

#include "../DerictoriesTestUI/testingclass.h"

class InitialTest : public QObject
{
    Q_OBJECT

public:
    InitialTest();
    ~InitialTest();

    TestingClass tc;

private slots:
    void test_case1();
    void test_case2();

};

InitialTest::InitialTest()
{

}

InitialTest::~InitialTest()
{

}

void InitialTest::test_case1()
{
    QCOMPARE(tc.GetMax(1, 2), 2);
    QCOMPARE(tc.GetMin(1, 2), 1);
}

void InitialTest::test_case2()
{
    QCOMPARE(tc.GetMin(1, 2), 1);
}

QTEST_MAIN(InitialTest)

#include "tst_initialtest.moc"
